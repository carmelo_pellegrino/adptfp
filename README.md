# Advanced Descent PostTrigger File Parser

This program parses and prints out almost all informations contained into
a NEMO/Phase3 post-trigger file.

# How to install
## Download the code
```
#!shell
git clone https://carmelo_pellegrino@bitbucket.org/carmelo_pellegrino/adptfp.git
```
## Dependencies
You need to have installed in your computer at least:

* CMake version >= 2.6 (only for building) 
* The Boost C++ libraries version >= 1.53

## Build
```
#!shell
mkdir -p build
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Release
make
```
# Usage
Typing on your terminal:
```
#!shell
cd build
./pt_straight
```
you will see the message `Usage: ./pt_straight ptfile [verbosity level = 1]`. The verbosity level is an integer parameter (default = 1) that toggles the amount of output printed to the screen following the scheme:

* verbosity = 1 -> print the header of the file
* verbosity = 2 -> print the header of the file and all TS headers
* verbosity = 3 -> same as 2 plus the event headers
* verbosity = 4 -> same as 3 plus the data frame headers

