#include <iostream>
#include <f_dataformat_p3.hpp>
#include <fstream>
#undef NDEBUG
#include <cassert>
#include <stdexcept>
#include <boost/lexical_cast.hpp>

using namespace tridas::phase3;

template<typename Header>
Header readHeader(std::ifstream& input)
{
  Header head;
  input.read(
      static_cast<char*>(static_cast<void*>(&head)),
      sizeof(head));

  if (input.gcount() != sizeof(head)) {
    throw std::runtime_error("Error reading the full header from file.");
  }

  return head;
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " ptfile [verbosity level = 1]\n";
    return EXIT_FAILURE;
  }

  int verbosity = 1;

  if (argc > 2) {
    verbosity = boost::lexical_cast<int>(argv[2]);
  }

  std::ifstream data(argv[1]);

  PTHeader const pt_head = readHeader<PTHeader>(data);

  data.ignore(pt_head.datacard_size);

  std::cout << pt_head << '\n';

  unsigned int ts_count = 0;

  while (data && ts_count < pt_head.number_of_timeslices) {
    TimeSliceHeader const ts_head = readHeader<TimeSliceHeader>(data);

    if (verbosity > 1) {
      std::cout << ts_head << '\n';
    }

    unsigned int event_count = 0;

    while (data && event_count < ts_head.NEvents) {
      TEHeaderInfo const tev_head = readHeader<TEHeaderInfo>(data);

      if (verbosity > 2) {
        std::cout << tev_head << '\n';
      }

      assert(tev_head.EventTag == 12081972);

      unsigned int hit_count = 0;

      int to_be_read = tev_head.EventL - sizeof(tev_head);

      while (data && (hit_count < tev_head.nHit || to_be_read > 0)) {
        DataFrameHeader const df_head = readHeader<DataFrameHeader>(data);
        to_be_read -= sizeof(df_head);

        assert(testDFHSync(df_head));

        if (verbosity > 3) {
          std::cout << df_head << '\n';
        }

        data.ignore(getDFHPayloadSize(df_head));

        assert(data.gcount() == getDFHPayloadSize(df_head));
        to_be_read -= getDFHPayloadSize(df_head);

        if (!subsequent(df_head)) {
          ++hit_count;
        }
      }

      assert(!to_be_read);

      assert(hit_count == tev_head.nHit);
      ++event_count;
    }

    assert(event_count == ts_head.NEvents);
    ++ts_count;
  }

  assert(ts_count == pt_head.number_of_timeslices);
}
